import React from 'react';

interface ITestComponentProps {
    color: 'red' | 'green' | 'blue';
}

export const TestComponent = (props: ITestComponentProps) => {
    return <li style={{ color: props.color }}>Das ist ein Testcomponent mit der Farbe: {props.color}</li>;
};

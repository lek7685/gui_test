import 'bootstrap/dist/css/bootstrap.min.css';
import React from 'react';
import { Alert, Button, Card, Dropdown, ProgressBar, Spinner, Tab, Tabs, Toast } from 'react-bootstrap';

export const Bootstrap = () => {
    return (
        <>
            <Button>Test</Button>
            <Alert variant="success">This is a alert—check it out!</Alert>
            <Alert variant="danger">This is a alert—check it out!</Alert>
            <Card style={{ width: '18rem' }}>
                <Card.Body>
                    <Card.Title>Card Title</Card.Title>
                    <Card.Text>Some quick example text to build on the card title and make up the bulk of the card's content.</Card.Text>
                    <Button variant="primary">Go somewhere</Button>
                </Card.Body>
            </Card>
            <Dropdown>
                <Dropdown.Toggle variant="success" id="dropdown-basic">
                    Dropdown Button
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    <Dropdown.Item>Action</Dropdown.Item>
                    <Dropdown.Item>Another action</Dropdown.Item>
                    <Dropdown.Item>Something else</Dropdown.Item>
                </Dropdown.Menu>
            </Dropdown>
            <ProgressBar now={60} style={{ marginTop: '20px' }} label={`60%`} animated />
            <ProgressBar now={60} style={{ marginTop: '20px' }} label={`60%`} variant="success" />
            <Spinner animation="border" />
            <Tabs defaultActiveKey="profile" id="uncontrolled-tab-example">
                <Tab eventKey="home" title="Home">
                    test
                </Tab>
                <Tab eventKey="profile" title="Profile">
                    t
                </Tab>
            </Tabs>
            <Toast>
                <Toast.Header>
                    <strong className="mr-auto">Bootstrap</strong>
                    <small>11 mins ago</small>
                </Toast.Header>
                <Toast.Body>Hello, world! This is a toast message.</Toast.Body>
            </Toast>
        </>
    );
};

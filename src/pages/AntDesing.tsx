import { ArrowUpOutlined } from '@ant-design/icons';
import { Alert, Button, Card, DatePicker, Dropdown, Input, Menu, message, Progress, Slider, Space, Statistic, Switch, Tabs } from 'antd';
import 'antd/dist/antd.css';
import Search from 'antd/lib/input/Search';
import React from 'react';

export const AntDesign = () => {
    return (
        <Space>
            <Space direction="vertical">
                <Button type="primary">Test</Button>
                <Dropdown
                    overlay={
                        <Menu>
                            <Menu.Item>Item #1</Menu.Item>
                            <Menu.Item>Item #2</Menu.Item>
                        </Menu>
                    }
                    placement="bottomLeft"
                >
                    <Button>Dropdown</Button>
                </Dropdown>
                <Input placeholder="Eingabe" />
                <Search placeholder="Eingabe" enterButton />
                <Slider />
                <Switch />
                <DatePicker.RangePicker />
                <Card title="Karte">Test test</Card>
                <Card>
                    <Statistic title="Mit Statikstik" value={11.28} precision={2} valueStyle={{ color: '#3f8600' }} prefix={<ArrowUpOutlined />} suffix="%" />
                </Card>
                <Tabs defaultActiveKey="1">
                    <Tabs.TabPane tab="Tab 1" key="1">
                        Content of Tab Pane 1
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Tab 2" key="2">
                        Content of Tab Pane 2
                    </Tabs.TabPane>
                    <Tabs.TabPane tab="Tab 3" key="3">
                        Content of Tab Pane 3
                    </Tabs.TabPane>
                </Tabs>
                <Alert message="Success Text" type="success" />
                <Button type="primary" onClick={() => message.info('This is a normal message')}>
                    Display normal message
                </Button>
            </Space>
            <Space direction="vertical" style={{ marginLeft: '50px' }}>
                <Progress percent={30} />
                <Progress percent={100} />
                <Progress percent={100} status="exception" />
                <Progress type="circle" percent={66} />
                <Progress type="circle" percent={100} />
            </Space>
        </Space>
    );
};

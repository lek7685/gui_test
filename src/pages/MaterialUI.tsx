import { Badge, Button, Checkbox, Fab, FormControlLabel, Grid, LinearProgress, MenuItem, Radio, RadioGroup, Select, Slider, TextField } from '@material-ui/core';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import AddIcon from '@material-ui/icons/Add';
import DraftsIcon from '@material-ui/icons/Drafts';
import InboxIcon from '@material-ui/icons/Inbox';
import MailIcon from '@material-ui/icons/Mail';
import React from 'react';

export const MaterialUI = () => {
    return (
        <div style={{ display: 'flex' }}>
            <div style={{ width: '15%' }}>
                <Grid container direction="column" justify="space-between">
                    <Button color="primary" variant="contained">
                        Test
                    </Button>
                    <Checkbox inputProps={{ 'aria-label': 'primary checkbox' }} />
                    <Fab color="primary" aria-label="add">
                        <AddIcon />
                    </Fab>
                    <Select>
                        <MenuItem value={10}>Ten</MenuItem>
                        <MenuItem value={20}>Twenty</MenuItem>
                        <MenuItem value={30}>Thirty</MenuItem>
                    </Select>
                    <Slider aria-labelledby="continuous-slider" />
                    <TextField id="standard-basic" label="Standard" />
                    <LinearProgress variant="determinate" value={10} style={{ marginTop: '30px' }} />
                    <RadioGroup aria-label="gender" name="gender1">
                        <FormControlLabel value="female" control={<Radio />} label="Female" />
                        <FormControlLabel value="male" control={<Radio />} label="Male" />
                        <FormControlLabel value="other" control={<Radio />} label="Other" />
                        <FormControlLabel value="disabled" disabled control={<Radio />} label="(Disabled option)" />
                    </RadioGroup>
                    <Card>
                        <CardContent>
                            <Typography color="textSecondary" gutterBottom>
                                Word of the Day
                            </Typography>
                            <Typography variant="h5" component="h2">
                                benevolent
                            </Typography>
                            <Typography color="textSecondary">adjective</Typography>
                            <Typography variant="body2" component="p">
                                well meaning and kindly.
                                <br />
                                {'"a benevolent smile"'}
                            </Typography>
                        </CardContent>
                        <CardActions>
                            <Button size="small">Learn More</Button>
                        </CardActions>
                    </Card>
                    <div style={{ marginTop: '30px' }}>
                        <Badge badgeContent={4} color="error">
                            <MailIcon />
                        </Badge>
                    </div>
                </Grid>
            </div>
            <div>
                <List component="nav" aria-label="main mailbox folders">
                    <ListItem button>
                        <ListItemIcon>
                            <InboxIcon />
                        </ListItemIcon>
                        <ListItemText primary="Inbox" />
                    </ListItem>
                    <ListItem button>
                        <ListItemIcon>
                            <DraftsIcon />
                        </ListItemIcon>
                        <ListItemText primary="Drafts" />
                    </ListItem>
                </List>
            </div>
        </div>
    );
};

import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { TestComponent } from '../components/TestComponent';
import { IRootReducer } from '../redux/RootStore';
import './Default.css';

interface ICatFact {
    createAt: string;
    deleted: boolean;
    source: string;
    status: {
        verified: boolean;
        sentCount: number;
        feedback: string;
    };
    text: string;
    type: string;
    updatedAt: string;
    used: boolean;
    user: string;
}

export const Default = () => {
    const dispatch = useDispatch();

    const [counter, setCounter] = useState(0);
    const [facts, setFacts] = useState<ICatFact[]>([]);

    const reduxCounter = useSelector((root: IRootReducer) => root.counter);

    useEffect(() => {
        setFacts([]);
        fetch('https://cat-fact.herokuapp.com/facts')
            .then((result) => new Promise((r) => setTimeout(() => r(result), 1000)))
            .then((result: any) => result.json())
            .then((data: ICatFact[]) => {
                setFacts(data);
            });
    }, []);

    return (
        <>
            <h4>React Komponenten Struktur</h4>
            <div>
                <ul>
                    <TestComponent color="blue" />
                    <TestComponent color="red" />
                    <TestComponent color="green" />
                </ul>
            </div>
            <h4>React useState</h4>
            <div>
                Zähler: {counter}
                <button onClick={() => setCounter(counter + 1)} style={{ marginLeft: '10px' }}>
                    Erhöhen
                </button>
                <button onClick={() => setCounter(0)} style={{ marginLeft: '10px' }}>
                    Reset
                </button>
            </div>
            <h4>React useEffect + Typescript</h4>
            <div>
                {facts.length === 0
                    ? 'Lädt...'
                    : facts.map((v: ICatFact, i: number) => (
                          <p key={i}>
                              Fakt #{i + 1}: {v.text}
                          </p>
                      ))}
            </div>
            <h4>Redux Counter</h4>
            <div>
                Zähler: {reduxCounter}
                <button onClick={() => dispatch({ type: 'INCREASE' })} style={{ marginLeft: '10px' }}>
                    Erhöhen
                </button>
                <button onClick={() => dispatch({ type: 'RESET' })} style={{ marginLeft: '10px' }}>
                    Reset
                </button>
            </div>
        </>
    );
};

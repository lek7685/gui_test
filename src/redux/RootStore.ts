import { combineReducers, createStore } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { CounterReducer } from './CounterReducer';

export interface IRootReducer {
    counter: number;
}

const rootReducer = combineReducers<IRootReducer>({
    counter: CounterReducer
});

export const rootStore = createStore(rootReducer, composeWithDevTools());

import { Action } from 'redux';

export const CounterReducer = (state: number = 0, action: Action) => {
    switch (action.type) {
        case 'INCREASE':
            return state + 1;
        case 'RESET':
            return 0;
        default:
            return state;
    }
};

/*export const CounterReducer = (state: number = 0, action: CounterAction) => {
    return CounterActions.match(action, { INCREASE: () => (state += 1), RESET: () => (state = 0), default: () => state });
};


export const CounterActions = unionize(
    {
        INCREASE: ofType(),
        RESET: ofType()
    },
    {
        tag: 'type',
        value: 'payload'
    }
);

export type CounterAction = UnionOf<typeof CounterActions>;
*/

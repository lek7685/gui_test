import React from 'react';
import { Redirect, Route, Router, Switch, useHistory } from 'react-router';
import { AntDesign } from './pages/AntDesing';
import { Bootstrap } from './pages/Bootstrap';
import { MaterialUI } from './pages/MaterialUI';

import './Application.css';
import { Default } from './pages/Default';

export const Application = () => {
    const history = useHistory();

    return (
        <>
            <div className="btn-group">
                <button onClick={() => history.push('/')}>Default</button>
                <button onClick={() => history.push('/ui')}>MaterialUI</button>
                <button onClick={() => history.push('/antd')}>AntDesign</button>
                <button onClick={() => history.push('/boot')}>Bootstrap</button>
            </div>
            <div className="categories">
                <Switch>
                    <Route path="/" exact render={() => <Default />} />
                    <Route path="/ui" exact render={() => <MaterialUI />} />
                    <Route path="/antd" exact render={() => <AntDesign />} />
                    <Route path="/boot" exact render={() => <Bootstrap />} />
                    <Redirect to="/" />
                </Switch>
            </div>
        </>
    );
};

import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, HashRouter } from 'react-router-dom';
import { Application } from './Application';
import { rootStore } from './redux/RootStore';
import { Provider } from 'react-redux';

ReactDOM.render(
    <HashRouter>
        <Provider store={rootStore}>
            <Application />
        </Provider>
    </HashRouter>,
    document.getElementById('root')
);
